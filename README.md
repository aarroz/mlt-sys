Raw bindings for MLT. Generated using bindgen.
Requires ``mlt-devel`` to build. Get the appropriate package for your distro.
Currently tested on Solus and Debian

Working on easier use for other projects.

After running `cargo build` you can run `cargo doc --open` to see the interfaces.

Feel free to test them or write something with them.