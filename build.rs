extern crate os_type;

fn main() {
    match os_type::current_platform().os_type {
    os_type::OSType::Ubuntu => {
        println!("cargo:rustc-link-lib=libmlt6");
    }
    os_type::OSType::Debian => {
        println!("cargo:rustc-link-lib=libmlt6");
    }
    os_type::OSType::Redhat => {
        println!("cargo:rustc-link-lib=mlt");
    }
    os_type::OSType::Unknown => {
        println!("cargo:rustc-link-lib=mlt");
    }
    os_type::OSType::OSX => {
        println!("Not currently supported.");
    }
    os_type::OSType::Arch => {
        println!("cargo:rustc-link-lib=mlt");
    }
    os_type::OSType::CentOS => {
        println!("cargo:rustc-link-lib=mlt");
    }
    }
}

